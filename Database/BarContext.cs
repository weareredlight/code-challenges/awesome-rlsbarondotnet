﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;

namespace RLS.AwesomeBar.API.Database
{
    public class BarContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }

        public BarContext(DbContextOptions<BarContext> options)
        : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var jsonString = File.ReadAllText("./Database/Seed/cocktails.json");
            
            var seedData = JsonSerializer.Deserialize<List<SeedItem>>(jsonString);

            foreach (var category in seedData.Select(s => s.Category).Distinct())
            {
                modelBuilder.Entity<Category>().HasData(new Category
                {
                    Name = category,
                    CreatedAt = DateTime.UtcNow,
                });
            }

            foreach (var ingredient in seedData.SelectMany(s => s.Ingredients).Distinct())
            {
                modelBuilder.Entity<Ingredient>().HasData(new Ingredient
                {
                    Name = ingredient,
                    CreatedAt = DateTime.UtcNow,
                });
            }
        }
    }
}
